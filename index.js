const mysql = require("mysql");
const connection = mysql.createConnection({
    host: "localhost",
    database: "routes",
    user: "",
    password: ""
});

connection.connect();

const express = require('express');
const app = express();

const customer = require("./customer");

app.use((req, res) => {
   loadRoute(req, res);
})

const http = require("http").Server(app);
http.listen(4400);

/**
 * @summary load a route by parsing the req.path and then getting the correct route
 * from the database. once loaded from the databse, if the route exists then pass on
 * to the appropriate function which is calculated from the databse record returned
 * @param {*} req 
 * @param {*} res 
 */
function loadRoute(req, res) {
    const route = req.path;
    
    const document = route.split("/")[1];
    const action = route.split("/")[2];
    
    /**
     * if there isn't both a a document or an action then return and error
     * otheriwse load the record from the databse
     */
    if (!document || !action) {
        res.send("Cannot find action for this command");
        return;
    } else {
        let queryString = `select * from route where document = "${document}" and action = "${action}";`;

        connection.query(queryString, (err, response, fields) => {
            // if there is an error in the db or there is no response then return an error message
            if (err || !response) {
                res.send("Cannot find action for this command");
                return;
            }
            
            const jsonResponse = JSON.parse(JSON.stringify(response));
            
            // if the length of the result is 0 then return an error message
            // this means that no matching record was found
            if (jsonResponse.length <= 0) {
                res.send("Cannot find action for this command");
                return;
            }
            
            let action = jsonResponse[0].function;
            
            // if there is no matching fucntion for the function on the record then return an error
            if (!customer[action]) {
                res.send("Cannot find action for this command");
                return;
            }

            // move to the correct function
            customer[action](req, res);
        });
    }
}
