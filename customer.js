function create(req, res) {
    res.json({message: `Customer created!`});
}

function update(req, res) {
    res.json({message: `Customer updated!`})
}

function get(req, res) {
    res.json({message: `Here is a list of customers`})
}

function deleter(req, res) {
    res.json({message: `Customer deleted!`})
}

module.exports = {
    create,
    update,
    get,
    deleter
}