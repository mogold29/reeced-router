# Reeced Router

Reeced Router is a routing management tool to manage routing within express apps by storing the routes in the database rather than hardcoding the routes

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install the dependencies.

```bash
npm install
```

## Usage
This is part of r-framework. To use this standalone, set up the logic files to export to a single file and then reference the export file. In the index.js file on line 71 add another varibale for the document so the final code should be
```bash
routes[document][action](req, res);
```
All functions called from a route should have two parameters of req and res


[ISC](https://opensource.org/licenses/ISC)