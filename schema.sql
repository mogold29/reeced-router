create database routes;

use routes;

create table route (
    "document" varchar(20) not null,
    "action" varchar(20) not null,
    "permissionDocument" varchar(50) not null,
    "permissionAction" varchar(50) not null,
    "class" varchar(100) not null,
    "function" varchar(100) not null,
    "routeId" varchar(48) not null,
    primary key ("routeId")
);

insert into route values("customer", "create", "customer", "create", "customerClass", "create", "4b0f1052-e71e-4d88-81e4-2091f363e76b");
insert into route values("customer", "update", "customer", "update", "customerClass", "update", "719db680-0b5d-4008-99fa-dd65b8ce81d2");
insert into route values("customer", "deleter", "customer", "deleter", "customerClass", "deleter", "99b34006-89e8-4bb5-82b9-a0b5c5c1a8f3");
insert into route values("customer", "get", "customer", "get", "customerClass", "get", "6c87a005-61ed-4ef2-8234-ba1b1fed5202");